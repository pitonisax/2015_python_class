import sqlite3

# Create a connection to sqlite
conn = sqlite3.connect('test.db')

# Open a cursor
c = conn.cursor()

# Execute any SQL statement
# Create table named "stocks" if it does not already exist.
# with the name of the columns: name, data type.  3 are text,and 2 are real numbers
c.execute('''create table if not exists stocks (date text, trans text, 
			symbol text, qty real, price real)''')

# Add entries
c.execute("insert into stocks values ('2006-02-05','SELL','RHAT',50,50.25)")
c.execute("insert into stocks values ('2006-03-15','BUY','GOOG',1000,350.13)")
c.execute("insert into stocks values ('2006-10-05','BUY','GOOG',100000,400.14)")
conn.commit()

# Query all the entries in the database
alllines = c.execute('select * from stocks')
print alllines

# Print it
for lines in alllines:
    print lines

c.close()
