from numpy import *
import time

def taylor():
    #Start the clock
    t1 = time.clock()

    
    #No of terms in the series
    noofterms = 100000
    
    #Calculating the numerator. The first few terms are 1, -1, 1, -1 ...
    numerator = ones(noofterms)
    for i in range(1,noofterms):
        numerator[i] = pow(-1,i)   
	
    # Calculating the denominator. First few terms are 1,3,5,7 ...
    # linspace(a,b,n) gives n equally spaced numbers between a and b.
    # Since we need terms 1,3,5,7 etc. where each term is 2 units
    # apart from its predecessor, we start with 1 and go on until 
    # noofterms*2 for number of terms equal to noofterms.
    denominator  = linspace(1,noofterms*2,noofterms)
  
    #Find the ratio and sum all the fractions to obtain pi value
    pi_value =  sum(numerator/denominator)*4.0
    print "pi_value = %f" % pi_value
    t2 = time.clock()
    
    #Determine the time for computation
    timetaken = t2-t1
    print "Timetaken = %0.2f seconds" % timetaken
    
    
if __name__ == '__main__':
    taylor()


