__author__ = 'ldconejo'
import csv

class WordCounter():

    def __init__(self,filename):
        # Opens filename and returns a list with all lines in the file
        self.f = open(filename, 'r')

    def __str__(self):
        return str(self.lines)

    def removepunctuation(self):
        self.lines = []
        for item in self.f.readlines():
            # Remove carriage returns
            line = item.strip()
            # Check if line if empty (after removal of carriage return)
            if len(line) <> 0:
                # Remove commas and dots
                line = line.translate(None, '.,"')
                # Split into individual words
                line = line.split(' ')
                self.lines.extend(line)
        # Clear any empty list elements caused by dual spaces in the source text
        for item in self.lines:
            if item == '':
                itemIndex = self.lines.index(item)
                del(self.lines[itemIndex])

        print self.lines

    def findcount(self):
        # Get unique words
        self.unique = set(self.lines)
        self.frequency = {}
        for key in self.unique:
            count = self.lines.count(key)
            self.frequency[key] = count
        print self.frequency

    def writecountfile(self,csvfilename):
        with open(csvfilename,'wb') as csvfile:
            linewriter = csv.writer(csvfile)
            for key, value in self.frequency.iteritems():
                #print key
                linewriter.writerow([key, value])
        print "INFO: CSV file created"

a = WordCounter('red-headed-league.txt')
a.removepunctuation()
a.findcount()
a.writecountfile('test.csv')