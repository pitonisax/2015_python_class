#!/usr/bin/python
'''
Homework #2 by Karla Reyes
August 7'2015

Question 2: 3 points

A queue follows FIFO (first-in, first-out). FIFO is the case where the first element added is the first element
that can be retrieved. Consider a list with values [1,2,3]. Create a class called MyQueue that will have two
methods: queueadd and queueretrieve to add and retrieve elements from the list in FIFO order respectively.
After each function call, print the content of the list.Add 7 to the queue and then follow the FIFO rules
to retrieve an element.

Your call to the class will be as follows
a = [1,2,3]
q = MyQueue(a)
q.queueadd(7)
print q
q.queueretrieve()
print q

The output on the screen should similar to
1 2 3 7
2 3 7
'''
class Myqueue():
    # def __init__(self, items):
    #     self.in_stack = []

    #     pass
    def queueadd(self,items):
        self.items.append(items)
    def queueretrieve(self):
        return self.items[len(self.items)-1]

a = [1,2,3]
q = Myqueue(a)
q.queueadd(7)

print q
# q.queueretrieve()
# print q