__author__ = 'ldconejo'

#class MyQueue():

# def __init__(self, testlist):
#     self.testlist = testlist
#     pass

# Adds new elements at the end of the list
def queueAdd(element):
    self.testlist.append(element)

def queueRetrieve():
    self.testlist.pop(0)

# This one will allow printing the list when a print is called on the class instance
def __str__(self):
    return str(self.testlist)

# Main execution
# a = [1,2,3]
# q = MyQueue(a)
# q.queueAdd(7)
# print q
# q.queueRetrieve()
# print q
#
# f = open('test.txt', 'r')
# f.readline()

