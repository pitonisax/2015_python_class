#!/usr/bin/python
'''
Homework #2 by Karla Reyes
August 7'2015
Question 1: 7 points

You are to write a program that will read the file, 'red-headed-league.txt', count the frequency of the words and store it as a csv file.

You need to create a class called WordCounter with the following methods.

def __init__(self,filename) where filename is the name of the text file, 'red-headed-league.txt'. This function should read the text file

def removepunctuation(self) must remove all the punctuations and leave only alphabets and numbers in each word

def findcount(self) must count the frequency of each word and store it in a instance variable called countdict

def writecountfile(self,csvfilename) writes the content of the countdict variable to a csv file with two columns. The first column is the word and second column is the count.

You need to create an instance of the class and call appropriate method and store the result in a csv file.
 '''

import csv

# class WordCounter:

#  where filename is the name of the text file, 'red-headed-league.txt'. This function should read the text file
# def __init__(self,filename):
file=open('red-headed-league.txt','r')

# must remove all the punctuations and leave only alphabets and numbers in each word
# def removepunctuation(self):

non_stripped_words = []
for words in file.readlines():
    non_stripped_words.extend(words.split(' '))

#print stripped_words
stripped_words = []
for item in non_stripped_words:
    item = item.strip()
    item = item.strip('''!()-[]{};:'"\,<>./?@#$%^&*_~''')
    if len(item) > 0:
        stripped_words.append(item)

# must count the frequency of each word and store it in a instance variable called countdict
# def findcount(self):
countdict={}
for word in stripped_words:
    if word not in countdict:
        countdict[word] = 1
    else:
        countdict[word] += 1
# print countdict

# writes the content of the countdict variable to a csv file with two columns.
 #def writecountfile(self,csvfilename):

with open('hw2output.csv', 'w') as csvfile:
    list_key_value =  [ [W,C] for W, C in countdict.items() ]
    # print list_key_value
    fieldnames = ['Word', 'Count']
    csvWriter = csv.writer(csvfile)
    # csvWriter = csv.writer(csvfile, fieldnames=fieldnames)
    # csvWriter.writeheader()
    csvWriter.writerows(list_key_value)

file.close()