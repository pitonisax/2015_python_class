# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

'''
Data types

1. List
2. Dictionary
3. tuples
4. sets
5. strings
6. slicing lists, strings and tuples
7. List comprehension
https://docs.python.org/2/tutorial/datastructures.html
'''    

# <codecell>

# Syntax for list is n = [item1, item2, item3].  The items can be any Python object
# the index of the list starts from zero
mylist = [1,2,"CA",[3,4]]  # mylist is a collection of numbers, string and a list
print mylist

# <codecell>

mylist[0] # this returns the first element in mylist

# <codecell>

mylist[-2] # this returns the last but one element in mylist

# <codecell>

# mylist[a:b] will return elements with index starting from a up to b-1. 
# This process is called Slicing
mylist[0:5]

# <codecell>

print mylist[-1][1] #this is used to get elements from a list of list

# <codecell>

# append will add a value to the end of the list
mylist.append(3.14) 
print mylist

# <codecell>

# Pop removes an element at the given index
print mylist.pop(2)
print mylist

# <codecell>

# Insert adds an element at the given index
mylist.insert(2,"MN")
print mylist

# <codecell>

print mylist[1]

# <codecell>

mylist = ['3','3a',1,5,2,2.4] # note '3' is a string
a = mylist.sort()
print a, mylist

# <codecell>

# sort() function will sort the list in ascending order. 
# If reverse = True is specified then it will sort the list on descending order
mylist.sort(reverse=True)
print mylist

# <codecell>

# for list of lists, sort command sorts the lists with respect to the first element 
# in the lists
a = [[10,3],[5,2]]
a.sort()
print a

# <codecell>

b = [['q', 'a'], ['dog','cat']]
b.sort()
print b

# <codecell>

mylist = [2,3,2,5,6,6,6]
mylist.count(6) # count will return the frequency of occurance of a particular value in the list

# <codecell>

mylist = [1,2,3]
mylist_copy1 = mylist
mylist_copy1.extend([4,5,6])
print mylist
print mylist_copy1
#print "mylist extend",mylist_copy1
#print mylist,len(mylist)

# <codecell>

# What is wrong with the next two statements?
mylist = [1,2,3]
mylist_copy = mylist
print mylist
print mylist_copy
mylist_copy.append([4,5,6])
#print "mylist append",mylist_copy
print mylist,len(mylist)
print mylist_copy,len(mylist_copy)
# len(object) will return the number of items of an object

# <codecell>

'''
Note 1: mylist.append([4,5,6]) will add this list to the end of mylist. Whereas 
mylist.extend([4,5,6]) will add values 4, 5 and 6 to mylist.

Note 2: mylist_copy1 is a copy of mylist. Any changes to mylist_copy1 will also 
affect mylist because the copy we made is a shallow copy.
'''

# <codecell>

#### DEEP COPY
mylist = [5,9,3]
mylist_copy2 = mylist[:]
mylist_copy2.append([11,2,3])
#mylist.sort()
#mylist_copy2.sort()
print mylist
print mylist_copy2
#print "mylist append",mylist_copy2
#print mylist,len(mylist)

# <codecell>

'''
Note: mylist_copy2 is a deep copy of mylist so any changes to mylist_copy2 will not affect 
mylist and vice versa.
'''

# <rawcell>

# # filter(), map(), and reduce()

# <codecell>

for i in range(2,10):
    if i%2!=0:
        print i

# <codecell>

# Get all odd numbers between 2 and 10
a = range(2,10)
print a

def f(x): 
    return x % 2 != 0

b = filter(f, a) 
# filter command takes two inputs one is a function and other one is iterable
# filter returns a list of values that satisfies the condition, in this case it returns all 
# the values between 2 and 9 that are NOT divisible by 2. 
print b,a

# <codecell>

def squared(x): 
    return x*x

map(squared, range(1, 5)) 
# map command takes a function and iterable(s). In this case one input is the 
# function and other input is the values. Map returns a list which is the output of the function. 
# In this case it returns the squares of the values from 1 to 4. Map takes a set of values and 
# returns another set of values based on the function.

# <codecell>

a = [1,2,3]
b = [4,5,6]
def add(a1, b1): return a1+b1
map(add,a,b)
# here map takes two lists and adds them

# <codecell>

# traditional method
squares = []
for x in range(10):
    squares.append(x**2)
print squares

# <codecell>

# smart method using list comprehension
squares = [x**2 for x in range(10)]
print squares

# <codecell>

# an elaborate example of list comprehension which combines two lists and returns a list of tuples
combined = [(x, y) for x in [1,2,3,4] for y in [3,1,4] if x != y and x>y]
print combined

# <codecell>

'''
Tuple is a collection of immutable Python elements. That means we cannot delete, add or modify elements
in a tuple. The syntax for a tuple is a = (item1,item2,item3,).  
'''

# <codecell>

T1 = 1,2,'String'
# Here Python will automatically assumes T1 is a tuple
print T1

# <codecell>

emptyTuple = ()
print emptyTuple

# <codecell>

T2 = (1,)
# without the comma, T2 will be of type integer and not tuple
print T2,type(T2)

# <codecell>

T2 = T2+1 # Not allowed, as tuple cannot be modified
print T2,type(T2)

# <codecell>

T3 = T1,T2 # T3 is a tuple of tuples
print T3
print T3[0]
print T3[0][1] 
# 0 index indicates the first tuple and 1 index indicates the 
# second element in the first tuple

# <codecell>

T3[0][0]=2 # New values cannot be assigned

# <codecell>

t  = (1,2,3)
def squared(x): return x*x
map(squared, t) 
# map operation on a tuple. This returns a list. Note map function always returns a list, 
# even if the input is not a list. 

# <codecell>

B = (1,) # B is a tuple with one element
B = (1,2,) # a new tuple named B is created which has two elements
B = () # a new tuple named B is created with no elements
print B

# <codecell>

A = (2,4,5,8,10,11)
B = ('CA', 'MN', 'TX')
C = A + B # + operation concatenates the tuples
print C

# <codecell>

# Basic tuple operations
print len(A) 
print max(A)
print min(A)

# <codecell>

# Basic tuple operations
H = ('Hello',)
print H*4 # * means repetition

# <codecell>

# Basic tuple operations
serial1 = ['Belize','Costa Rica','Guatemala']
serial2 = ['Ecuador', 'Belize','Nicaragua']
print cmp(serial1,serial1)
print cmp(serial1,serial2)
print tuple(serial1)

# <codecell>

'''
Inclass activity: Consider the tuple t1 = (2,4,8). 
1. Try to modify the first element of the tuple to 5. 
2.convert the tuple into a list and append the number 5 to the list
3. Use extend to add [1,4,5] to the list
4. Pop the third item from the list and then print the list.
5. Convert it back into a tuple. 
'''

# <codecell>

'''
Dictionary is another useful data type built into Python is the dictionary (see Mapping Types â dict). 
Dictionaries are sometimes found in other languages as âassociative memoriesâ or âassociative arraysâ. 
Unlike sequences, which are indexed by a range of numbers, dictionaries are indexed by keys, 
which can be any immutable type; strings and numbers can always be keys. Tuples can be used as keys 
if they contain only strings, numbers, or tuples; if a tuple contains any mutable object either 
directly or indirectly, it cannot be used as a key. You canât use lists as keys, since lists can 
be modified in place using index assignments, slice assignments, or methods like append() and extend(). 
https://docs.python.org/2/tutorial/datastructures.html#tuples-and-sequences
'''

# <codecell>


# <codecell>

ziploc = {}
print ziploc

# <codecell>

# syntax for a dictionary is dict = {'key1': 'value1', 'key2': 'value2'}
# dict is a key-value pair
ziploc = {'95117': 'San Jose','94086':'Sunnyvale','95014': 'Cupertino'}
# in this example for key 95117, San Jose is the value, for key 94086, Sunnyvale is the value
print ziploc
print ziploc.keys()
print tuple(ziploc.keys())
print ziploc.values()
print ziploc['95117']
ziploc['95117'] = 'Santa Clara'
print ziploc['95117']
ziploc['95054'] = 'Santa Clara'
print ziploc

# <codecell>

for keys in ziploc:
    print keys,ziploc[keys]

# <codecell>

for keys,value in ziploc.iteritems(): # Use .items() for version 3.0+
    print keys,value

# <codecell>

ziploc['55421'] # Not allowed, as the dictionary does not have key 55421

# <codecell>

exists = False
for k,v in ziploc.iteritems():
    if k == '95117':
        exists = True
        break
print exists

# <codecell>

'55421' in ziploc

# <codecell>

ziploc.items() # dict.items() returns a list of (key,value) tuple pairs

# <codecell>

ziploc.values() #dict.values() returns the dictionary values

# <codecell>

ziploc.clear() # removes all the elements of the dictionary
print ziploc

# <codecell>

squared = {x: x**2 for x in range(5)}
# we are using dictionary comprehension 
print squared

# <codecell>

'''
Set - is a unordered collection of unique items
'''

# <codecell>

vegetables = ['tomatoes','eggplant','cucumber','eggplant'] # A list
vegetables_set = set(vegetables)
# set command returns only unique items
print vegetables,vegetables_set

# <codecell>

a = list(vegetables_set)
# vegetables_set[0] # Not allowed as set cannot be indexed. You can iterate through them.
a[0]

# <codecell>

for items in vegetables:
    print items 

# <codecell>

for items in vegetables_set:
    print items 

# <codecell>

print 'Cucumber' in vegetables_set 
# the above statement checks whether Cucumber is in vegetables_set

# <codecell>

finallistofveg = list(vegetables_set)
print finallistofveg

# <codecell>

a = [1,1,4,5,7]
print type(a), a
b = set(a)
c = list(b)
print c

list(set(a))

