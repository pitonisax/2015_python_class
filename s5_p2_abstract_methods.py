# Function overloading
class Animal:
    name = 'Animal' # class variable
    def eat(self):
        print "Animal eating"
    def drink(self):
        print 'Animal drinking'
        
class Dog(Animal):
    name = 'Dog' # class variable
    def eat(self): 
        print "Dog eating"
    def drink1(self):
        print 'Dog drinking'
    
        
d = Dog()
d.eat()
print d.name
d.drink()
d.drink1()
d.sleep()

####
# This is an example of an abstract class created by importing a module called abc
import abc
class AbstractImage():
    __metaclass__ = abc.ABCMeta
    @abc.abstractmethod
    def read(self):
        return
    @abc.abstractmethod
    def write(self):
        return

ai = AbstractImage()
print ai # Will give an error that it cannot create an instance of the abstract class
#ai.read()


####
#9:03pm
# If the line @abc.abstractmethod is commented, you will notice that if the
# method is not implemented in the child, the parent class write method will be called.

import abc
class AbstractDicom():
    __metaclass__ = abc.ABCMeta
    @abc.abstractmethod
    def read(self):
        return
    @abc.abstractmethod
    def write(self):
        print "inside the base write method"
        return

class Dicom(AbstractDicom):
    def read(self):
        print "reading file"
    #def write(self):
        #print "writing file"

d = Dicom()
print d
d.read()
d.write()


