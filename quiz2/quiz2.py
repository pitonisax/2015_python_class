'''Write a function that will return True if a string is a palindrome. A palindrome is a word, phrase, number, or other sequence of characters which reads the same backward or forward according to Wikipedia.

For example the word, "radar" is a palindrome as it spelled the same when read forward or backward. Your python function should have a signature def ispalindrome(mystring) and must return True if the input string is a palindrome and false otherwise.

You can reverse the string using reversestring = mystring[::-1]

You are given a file 'palindrome.txt' with one word on each line. Read the file line-by-line and check and print if the word is a palindrome.
'''

# import random
# a = [1,3,5,6,12,93]
# print random.choice(a)


# file=open('palindrome.txt','r')
# Need to strip the spaces in the first 2 lines...

import sys
mystring = 'radar'
reversestring = ''

def ispalindrome(mystring):
    if reversestring = mystring[::-1]
    print 'True'

sys.argv[]