
''' Write a code that will take name from the user until the user decides to quit by entering q or Q. 
 Also perform a check to see whether the user entered anything. If the user didn't enter anything, 
 ask them to enter the name. '''

while True:
	x = raw_input("Please enter your name: ")
	if x:
		if (x == "q") or (x == "Q"):
			break
		else:
			print x
	else: 
		print 'you need to type a name'

#e#se:'enter the name'

# elif x == 0:
#     print "Zero"
# elif x == 1:
#     print 'One'
# elif x == 2:
#     print 'Two'
# elif x == 3:
#     print 'Three'
# else:'enter the name'