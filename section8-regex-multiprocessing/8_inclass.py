import re

# patterns = 'and'
# text = 'Python is a dynamically typed language and also has a simple syntax'
# print re.search(patterns, text)
# if re.search(patterns, text):
#     print 'There is a match'
# else:
#     print 'Found no match'

#########################

# pattern = 'and'
# text = 'Python is a dynamically typed language and also has a simple syntax'
#
# compare = re.search(pattern,text)
#
# s = compare.start() # start() returns the starting position of the match
# e = compare.end() # end() returns the ending position of the match
#
# print 'Found "%s" in "%s"  from %d to %d ' %(pattern,text,s,e)
# # prints "Found "and" in "Python is a dynamically typed language and also has a simple syntax"  from 39 to 42 "
# # It tells you which position this exists


###############################
# Tell me how many times the pattern '10' appears in 'mynumber' , 10 is no longer an integer but a string..
#
# mynumber = 1034567810378103
# pattern = 10
# mynumber_str = str(mynumber)  #we made it a string (instead of guessing), so that i can do regex...
# print mynumber_str
# pattern_str = str(pattern)
# count = 0
# #findall() function finds all the substrings of the input that match the pattern
# #without overlapping syntax re.findall(pattern, string)
# print re.findall(pattern_str,mynumber_str)
# for match in re.findall(pattern_str,mynumber_str):
#     count = count + 1
# print 'In the given text, %d occured  %d times' %(pattern, count)

###############################
# finditer() returns an iterator that produces match instances instead of the
# strings returned by findall
# syntax re.finditer(pattern, string)
#
# text = '1034567810378103'
# pattern = '45'
# count = 0
# print re.finditer(pattern,text)
# for match in re.finditer(pattern,text):
#     s = match.start()
#     e = match.end()
#     count = count + 1
#     print 'The pattern "%s" starts at %d and ends at %d ' %(pattern, s, e)
# print 'In the given text, "%s" occured  %d times' %(pattern, count)
#
# '''
# pattern = '78'
# output: <callable-iterator object at 0x1002b6ad0>
# The pattern "78" starts at 6 and ends at 8
# The pattern "78" starts at 11 and ends at 13
# In the given text, "78" occured  2 times '''
#
# '''
# pattern = '45'
# <callable-iterator object at 0x10f55cad0>
# The pattern "45" starts at 3 and ends at 5
# In the given text, "45" occured  1 times'''

###############################

# import re
# strval1 = 'Barack Obama, Michelle Obama, Joe Biden, Jill Biden'
# list1 = strval1.split(',')
# print list1
#
# #anything that ends with Obama
# for items in list1:
#     # firstname = re.match(r'(.*)Obama',items)
#     firstname = re.match(r'(.*) Biden',items)
#       #r is rastering, we are saying: any special character that might have special meaning, ignore it...
#     if firstname:
#         #print firstname.group(0)
#         # returns every element in the list that has Obama in it
#         print firstname.group(1)
#         # returns first name of the element in the list that has Obama in it

###############################
# The oposite, now you want anyting that matches the beg... (not the end/lastname)
strval = 'San Francisco, San Jose, San Carlos, Santa Clara, Sunnyvale, Cupertino'
strval_list = strval.strip().split(',') # converting strval into a list

b = []
for items in strval_list:
    allnames = re.match(r'San(.*)', items.strip())
    # returns a subset of the list which start with San
    if allnames:
        b.append(allnames.group(1))
print b