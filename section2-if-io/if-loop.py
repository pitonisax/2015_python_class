# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

'''
Basic statements
1. if-else
2. while
3. for
4. for with range and xrange
'''

# <codecell>

# Syntax of if-else
# if statement a:
#    execute statement(s) that are under this block
# else statment b:
#    execute statement(s) that are under this block

x = int(raw_input("Please enter your choice (1-3): "))
if x < 0:
    print 'Negative number not allowed'
elif x == 0:
    print "Zero"
elif x == 1:
    print 'One'
elif x == 2:
    print 'Two'
elif x == 3:
    print 'Three'
else:
    print 'Number more than 3 not allowed'

# <codecell>

'''
Truthiness in Python

https://www.udacity.com/wiki/cs258/truthiness-in-python

https://docs.python.org/release/2.5.2/lib/truth.html

'''

# <codecell>

a = []
if a == []:
    print "No value"

# <codecell>

a = [1]
if a==[]:
    print "No value"
else:
    print "Has value"

# <codecell>

a = {}
if a == {}:
    print "No value"

# <codecell>

a = None
if a == None:
    print "No value"

# <codecell>

if True:
    print "Truth"

# <codecell>

# Syntax of the for statement
# for each_item in something:
#     execute statement(s) that are under this block
fruits= ['apple', 'grapes', 'peach']
for f in fruits:
    print f

# <codecell>

# range returns a list of values
# range(n) will return a list of values from 0 to n-1

for i in range(len(fruits)):
    print i, fruits[i]

# <codecell>

# range(n1,n2) will return a list from n1 to n2-1
print range(2,7)

# <codecell>

# xrange creates an object with an iterator that returns the value on demand
# the demand is in the loop
for i in xrange(len(fruits)):
    print i, fruits[i]

# <codecell>

# f is a filehandler
f = open('c:\users\uschirav\Desktop\python_list.txt')
# readlines reads one line at a time from the file
# Syntax filehandle.readlines()
for line in f.readlines():
    print line.strip() 
    # strip removes newline, carriage return, space etc at the beginning and at the end of the line
f.close()

# <codecell>

'  the    life    is    good   '.strip()

# <codecell>

# os is an operating system module
import os
print os.getcwd() # gives information about the current working directory

# <codecell>

for i in range(len(fruits)):
   if i == 1:
        print "first fruit",i, fruits[i]
   elif i == 2:
        print "second fruit",i, fruits[i] 
   elif i == 3:
        pass 

# <codecell>

a = range(5)
print a

# <codecell>

a = range(5)
for i in a:
    print i

# <codecell>

a = range(5)
count = 0
# Syntax of While
# While something is true:
#     execute statement(s) that are under this block 
while True:
    print a[count]
    count = count+1
    if count == len(a):
        break # this statement breaks out of the loop

# <codecell>

for n in range(22, 30):
    if n % 3 == 0:
        print n, 'is the first multiple of 3'
        break
    else:
        print n, 'is not a multiple of 3'

# <codecell>

for n in range(22, 30):
    if n % 4 == 0:
        print n, 'is a multiple of 4'
        continue
    else:
        print n, 'is not a multiple of 4'

# <codecell>

a = 1
for i in range(5):
    a *= (i+1)
print a,i

# <codecell>

for item in []:
    pass # this statement will do nothing  

print item # Will give NameError. Why?

# <codecell>

some_list = [1, 3, 6, -2, 5, 8]
print some_list

# <codecell>

for items in some_list:
    if (items < 0):
        # we are iterating through some_list until we hit the first negative number
        print items
        break

# <codecell>

'''
Write a code that will take name from the user until the user decides to quit by entering q or Q. 
Also perform a check to see whether the user entered anything. If the user didn't enter anything, 
ask them to enter the name. 
'''

