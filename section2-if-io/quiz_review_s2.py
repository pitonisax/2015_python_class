# Overloading operators
# https://docs.python.org/2/reference/datamodel.html
class ComplexNumber:
    def __init__(self, a, b):
        self.a = a
        self.b = b

#if i dont add this __str__ class, it will give me an ex funny hex #..
    def __str__(self):
        return 'ComplexNumber = %d+i%d' % (self.a, self.b)  #or cn1.a , cn1.b

    def __add__(self,other):
        # python goes to here when it sees cn1+cn2
        return ComplexNumber(self.a + other.a, self.b + other.b)   #think of this as cn3

    #it can be written like this, same as previous class..
    def __add__(self,other):
        # python goes to here when it sees cn1+cn2
        cn3 = ComplexNumber(self.a + other.a, self.b + other.b)
        return cn3


    def __mul__(self,other):
         # python goes to here when it sees cn1*cn2
        newa = self.a * other.a - self.b * other.b
        newb = self.a * other.b + self.b * other.a
        return ComplexNumber(newa,newb)

#other is an instance of the 'complexNumber'
cn1 = ComplexNumber(3,5)
cn2 = ComplexNumber(5,-2)
# cn1 = ComplexNumber(5,8)
# cn2 = ComplexNumber(5,-2)
print cn1
print cn2
print cn1 * ComplexNumber(3,0)
print cn1*cn2