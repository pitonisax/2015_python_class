# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

'''
Input and output functions

1. stdout using print
2. formatted printing
3. stdin using 

'''

# <codecell>

# number.zfill(n) will put zeros so that the 
# total number of digits in the number is n 
# if the number has a negative in front then the negative sign 
# is also accounted for.
'314'.zfill(10)

# <codecell>

'-3.14'.zfill(10)

# <codecell>

print '{0} and {1}'.format('san jose', 'sacramento')

# <codecell>

 print '{0} and {1}'.format('san jose') # fails as we need to provide two values

# <codecell>

 print '{nearcity} and {farcity}'.format(nearcity='San jose', farcity='Sacramento')

# <codecell>

import math 
print 'PI = {0:.5f}'.format(math.pi) # 0:.5 means 5 digits after decimal 
print 'PI = %0.5f'%(math.pi) # The () is optional for one input

# <codecell>

a = 5
b = 3.25
print 'a = %d' %a

# <codecell>

a = 5
b = 3.25
c = '5.6'
print 'a = %d b = %0.2f c = %s' %(a,float(b),c)

# <codecell>

# Try change the path of the file
f = open('/Users/chityala/Desktop/python_list.txt')
print f
for line in f.readlines():
    print line.strip()

# <codecell>

# raw_input() gets input from the command line.
# In this example,we are getting the input from the user and assigning it to variable s.
s = raw_input('Enter a name : ')

# <codecell>

# In Python 2.x, this function takes only a Python command as an input
s = input('Enter a name :')

