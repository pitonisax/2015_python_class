#PRIVATE methods
class Customer:
    def __init__(self,name):
        self.name = name
        self.__balance = 0.0
        # __ (double underscore) before the instance variable will make the
        # variable private for the class

    def set_balance(self, balance=0.0):
        self.__balance = balance

    def withdraw(self, amount):
        self.__balance -=amount
        return self.__balance

    def deposit(self, amount):
        self.__balance +=amount
        return self.__balance

    def get_balance(self):
        # print __balance
        return self.__balance

b = Customer("Leo")
b.set_balance(1000)
# b.withdraw(500)
# b.deposit(2000)
print b.get_balance()
#print b.__balance
#this will fail with an attribute error as the variable/attribute __balance
#is private to the class