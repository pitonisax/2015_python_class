'''
CLASS VARIABLE -Shares the same value across all instances of the class.
'''
class Employee:
    empCount = 0 #Class variable. No self. prefix
    className = 'Employee' #Class variable. No self. prefix
    def __init__(self, name, salary):
        self.name = name
        self.salary = salary
        Employee.empCount +=1

    def displayCount(self):
        print "Total Employee %d" %Employee.empCount

#
    def displayEmployee(self):
        print "Name:   ", self.name,  ", Salary:   ", self.salary

emp1 = Employee("Tara", 20000)
emp2 = Employee("Zeera", 7000)
# emp3 = Employee("Cara", 5000)
# emp4 = Employee("John",4000)
emp2.displayEmployee()
emp2.displayCount()
print "Total Employee %d" % Employee.empCount

if(emp2.className == 'Employee'):
    print "I have a Employee class"