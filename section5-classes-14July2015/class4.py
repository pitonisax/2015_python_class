# Ejercicios de la clase de Julio 17, sobre classes_part1.ipynb

class Customer:

    '''The attributes for this class are name and balance '''

    def __init__(self,name):
        self.name = name
        self.balance = 0.0

    def set_balance(self, balance=0.0):
        self.balance = balance

    def withdraw(self, amount):
        self.balance -=amount
        return self.balance

    def deposit(self, amount):
        self.balance +=amount
        return self.balance
class Customer:
    def __init__(self,name):
        self.name = name
        self.__balance = 0.0
        # __ (double underscore) before the instance variable will make the
        # variable private for the class

    def set_balance(self, balance=0.0):
        self.__balance = balance

    def withdraw(self, amount):
        self.__balance -=amount
        return self.__balance

    def deposit(self, amount):
        self.__balance +=amount
        return self.__balance

    def get_balance(self):
        return self.__balance

b = Customer("Leo")
b.set_balance(1000)
b.withdraw(500)
b.deposit(2000)
print b.get_balance()
#print b.__balance
#this will fail with an attribute error as the variable/attribute __balance
#is private to the class

b = Customer("Leo")  # creating an instance of the class named Customer
b.set_balance(1000) # calling set_balance method and passing a value
# b.withdraw(500) # calling withdraw method and passing a value
# b.deposit(2000) # calling deposit method and passing a value
print "Customer name %s and balance %0.2f "%(b.name, b.balance)
