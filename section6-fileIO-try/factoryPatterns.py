# Factory pattern is a creational pattern which uses factory methods to deal with the problem of creating
#  objects without specifying the exact class of object that will be created - Wikipedia

import random

def myfactory(objtype):
        if objtype == 1: return Burger()
        if objtype == 2: return Fries()
        print "Bad menu choice: "

class Menu(object):
    pass

class Burger(Menu):
    def listitem(self): print("Burger")

class Fries(Menu):
    def listitem(self): print("Fries")

print "Choose a menu item"
print "1: Burger, 2: Fries"

choice = int(raw_input('Enter your choice number :'))

#f is an burger object (if i chose 1) or fries object (if i chose 2)
f = myfactory(choice)
f.listitem()
print type(f)

###########
# Factory pattern

import random

class Menu(object):
    @staticmethod
    def factory(objtype):
        if objtype == 1: return Burger()
        if objtype == 2: return Fries()
        print "Bad menu choice: "

class Burger(Menu):
    def listitem(self): print("Burger")

class Fries(Menu):
    def listitem(self): print("Fries")

print "Choose a menu item"
print "1: Burger, 2: Fries"

choice = int(raw_input('Enter your choice number :'))

f = Menu.factory(choice)
f.listitem()
#print type(f)

#I dont use the @staticmethod, then i have to create an object, like
# M= m()
# f=M.factory(choice)
# f.listitme()