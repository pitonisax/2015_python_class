# fo = open('python_list.txt', 'r')
# # print fo.readlines()
# # Why does the for-loop not produce any output ???
# for items in fo.readlines():
#     print 'inside for-loop',items
# fo.close()


# The with keyword is called the context manager
# It will close the file whether or not there is an exception
with open('python_list1.txt', 'r') as fo:
    for items in fo.readlines():
        print items.strip()
# close does not have to be called exclusively