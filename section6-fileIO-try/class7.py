try:
   import cPickle as pickle
except:
   import pickle

# mydict = [ { 'a':'1', 'b':2, 'c':3 } ]
# mydict2 = {'d':4,'e':5}
#
# f = open('pickle.ck','wb')
# pickle.dump(mydict,f) #this command dumps mydict into the file pickle.ck
# pickle.dump(mydict2,f) # this command dumps mydict2 into the file pickle.ck
# f.close()
#
# f = open('pickle.ck','rb')
# newdict = pickle.load(f)
# newdict2 = pickle.load(f)
# f.close()
#
# print 'Read values are:',
# print newdict
# print(newdict2)

# import shelve
# mydict = [ { 'a':'1', 'b':2, 'c':3 } ]
# mylist = ['apple', 'mango', 'pineapple']
# s = shelve.open('fruit.sv') # opens the shelve
# try:
#     s['first'] = mydict
#     s['second'] = mylist
# finally:
#     print s
#     s.close()


# In[ ]:

import shelve
s = shelve.open('fruit.sv','r')
try:
    newdict = s['first']
finally:
    s.close()
print newdict