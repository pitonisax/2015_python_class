# g = lambda x: x**x
# g(4)

#############
# a = [2,-4,80]
# g2 = lambda x: x+4
# b = [g2(x) for x in a]
# print b
# # Outputs --> [6, 0, 84]


##### example of reduce
#find the sum of all emelments in alist
# a= [3,2,5]
# print reduce(lambda x,y: x+y, a)
# # outputs ==> 10  which is 3+2=5, then 5+5 = 10

# ### kwargs and args
# def sum1(x,y):
#     a = x + y
#     return a
# print sum1(10, -2)
# # outputs 8


# def take1(*args):
#     print args
#     for i in args:
#         print i
#
# take1(-10)
# take1(1,2,3)

# Output
# *args takes any number of inputs..
# (-10,)   <-- the comma indicates a
# -10
# (1, 2, 3)
# 1
# 2
# 3
###########################
# this is a little more verbose, expects a = abe, b = cab, and it also takes any number of inputs.
def utake(**kwargs):
    print kwargs

utake(a = 'abc')
utake(a = 'abe', b = 'cab')

#Outputs key = value, key = value
# {'a': 'abc'}
# {'a': 'abe', 'b': 'cab'}
# they dont know how many inputs are comming, they just treat all inputs the same....