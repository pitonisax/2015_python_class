'''
Create an abstract base class called 'InterestCal'. Create a child class called
'CICal'. The 'CICal' class will inherit the abstract base class.
This child class will have three methods:
1) __init__ method,
2) a method called compcal that computes the compound interest
3) a method called compout that prints the compounded value.


Once all classes have been defined, the call to calculate and print the final
value must follow the code below.

c = CICal(1000,5,6)
c.compcal()
print c.compout()

'''
#p,y/n,r
#1000 initial principal
#5 number of years
#6 i s the rate of interst
#t/n  t is # of years, n is # of times in a year

#  A true abstract class will have abstact methods

import abc

class InterestCal():
    __metaclass__ = abc.ABCMeta
    @abc.abstractmethod
    def compcal(self):
        return
    @abc.abstractmethod
    def compout(self):
        return

class CICal(InterestCal):
    def __init__(self,P,y,r,n):
        self.P = P
        self.y = y
        self.r = float(r)/100.0
        self.n = n

    def compcal(self):
        #P*(1+r)**n
        rdn = self.r/self.n
        ymn = self.y*self.n
        prdn = 1+rdn
        print rdn, ymn, prdn
        self.value = self.P
        return
    def compout(self):

c=InterestCal.CICal(raw_input)

c = CICal(1000,5,6)
c.compcal()
print c.compout()
