# Syntax of if-else
# if statement a:
#    execute statement(s) that are under this block
# else statment b:
#    execute statement(s) that are under this block

x = int(raw_input("Please enter your choice (1-3): "))
if x < 0:
    print 'Negative number not allowed'
elif x == 0:
    print "Zero"
elif x == 1:
    print 'One'
elif x == 2:
    print 'Two'
elif x == 3:
    print 'Three'
else:
    print 'Number more than 3 not allowed'